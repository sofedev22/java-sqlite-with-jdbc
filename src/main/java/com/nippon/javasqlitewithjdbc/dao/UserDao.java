/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.javasqlitewithjdbc.dao;

import com.nippon.javasqlitewithjdbc.helper.DatabaseHelper;
import com.nippon.javasqlitewithjdbc.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nippon
 */
public class UserDao implements Dao<User> {

    @Override
    public User get(int id) {
        User user = null;
        String sql = "SELECT * FROM User WHERE USER_ID=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
//                user = new User();
//                user.setId(rs.getInt("USER_ID"));
//                user.setName(rs.getString("USER_NAME"));
//                user.setRole(rs.getInt("USER_ROLE"));
//                user.setGender(rs.getString("USER_GENDER"));
//                user.setPassword(rs.getString("USER_PASSWORD"));
                user = User.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }

    @Override
    public List<User> getAll() {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM User";
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public User save(User obj) {
        String sql = "INSERT INTO USER (USER_NAME,USER_GENDER,USER_PASSWORD,USER_ROLE)"
                + "VALUES (?,?,?,?)";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getGender());
            stmt.setString(3, obj.getPassword());
            stmt.setInt(4, obj.getRole());
            //System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return obj;
    }

    @Override
    public User update(User obj) {
        String sql = "UPDATE USER" + "SET USER_ID = ?,USER_NAME = ?,USER_GENDER = ?,USER_PASSWORD = ?,USER_ROLE = ?"
                + "WHERE USER_ID = ?";

        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getGender());
            stmt.setString(3, obj.getPassword());
            stmt.setInt(4, obj.getRole());
            stmt.setInt(5, obj.getId());
            //System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public int delete(User obj) {
        String sql = "DELETE FROM USER WHERE USER_ID=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<User> getAllOrderBy(String name, String order) {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM USER ORDER BY " + name + " " + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
//                User user = new User();
//                user.setId(rs.getInt("user_id"));
//                user.setName(rs.getString("user_name"));
//                user.setRole(rs.getInt("user_role"));
//                user.setGender(rs.getString("user_gender"));
//                user.setPassword(rs.getString("user_password"));
                User user = User.fromRS(rs);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public User getByName(String name) {
        User user = null;
        String sql = "SELECT * FROM user WHERE user_name=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = (User) User.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }

}
