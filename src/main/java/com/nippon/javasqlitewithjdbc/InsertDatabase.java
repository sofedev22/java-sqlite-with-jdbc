/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.javasqlitewithjdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Nippon
 */
public class InsertDatabase {

    public static void main(String[] args) {
        //Connect Database
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee3.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connect to SQLite  has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Insert
        String sql = "INSERT INTO CATEGORY(CATEGORY_ID, CATEGORY_NAME) VALUES (?,?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 3);
            stmt.setString(2, "Candy");
            int status = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Close Database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
