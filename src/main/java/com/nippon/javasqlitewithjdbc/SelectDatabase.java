/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.javasqlitewithjdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Nippon
 */
public class SelectDatabase {

    public static void main(String[] args) {
        //Connect Database
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee3.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connect to SQLite  has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Selection
        String sql = "SELECT * FROM CATEGORY";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                System.out.println(rs.getInt("CATEGORY_ID") + " " + rs.getString("CATEGORY_NAME"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Close Database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
