/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.javasqlitewithjdbc;

import com.nippon.javasqlitewithjdbc.dao.UserDao;
import com.nippon.javasqlitewithjdbc.model.User;

/**
 *
 * @author Nippon
 */
class UserService {

    public User login(String name, String password) {
        UserDao userdao = new UserDao();
        User user = userdao.getByName(name);
        if (user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }
}
